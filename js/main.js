$(document).ready(function () {

    $('.share__icon').click(function (e) {
        $(this).parent().toggleClass('active');
    })

    $('.burger').click(function () {
        $('.nav').addClass('active');
    })

    $('.nav').click(function () {
        $(this).removeClass('active');
    });

    $('.nav__link').click(function () {
        $('.nav__link').removeClass('active');
        $(this).addClass('active');
    })

    $('.btn-modal').click(function () {
        $('.modal-overlay').removeClass('modal-overlay--active');
    })

    $(document).on('click', 'a.nav__link[href^="#"] , .hero__btn', function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 1000);
    });

    $('.nav-el').each(function () {
        if ($(window).scrollTop() >= $(this).offset().top - 300) {
            var id = $(this).attr('id');

            if ($(this).hasClass('js-black-text')) {
                $('.nav a[href="#' + id + '"]').parent().parent().addClass('js-dark-text');
                $('.share').addClass('js-black-soc');
                $('.burger').addClass('js-black-burger');
            }

            else {
                $('.nav a[href="#' + id + '"]').parent().parent().removeClass('js-dark-text');
                $('.share').removeClass('js-black-soc');
                $('.burger').removeClass('js-black-burger');
            }

            $('.nav a').removeClass('active');
            $('.nav a[href="#' + id + '"]').addClass('active');
        }
    });

    $(window).on('scroll', function () {
        $('.nav-el').each(function () {
            if ($(window).scrollTop() >= $(this).offset().top - 300) {
                var id = $(this).attr('id');

                if ($(this).hasClass('js-black-text')) {
                    $('.nav a[href="#' + id + '"]').parent().parent().addClass('js-dark-text');
                    $('.share').addClass('js-black-soc');
                    $('.burger').addClass('js-black-burger');
                }

                else if ($(this).hasClass('js-black-text-mobile') && window.innerWidth < 768) {
                	$('.nav a[href="#' + id + '"]').parent().parent().addClass('js-dark-text');
                    $('.share').addClass('js-black-soc');
                    $('.burger').addClass('js-black-burger');
                }

                else {
                    $('.nav a[href="#' + id + '"]').parent().parent().removeClass('js-dark-text');
                    $('.share').removeClass('js-black-soc');
                    $('.burger').removeClass('js-black-burger');
                }

                $('.nav a').removeClass('active');
                $('.nav a[href="#' + id + '"]').addClass('active');
            }
        });
    });

    function articleSwitch() {

        function desktop() {
            $('.arcticles .acco__item-head').click(function () {

                var self = $(this);
                $('.arcticles .acco__item-head').removeClass('active');
                $('.arcticles .acco__item-head').removeClass('text');
                if (self.parent().hasClass('active')) {
                    $('.arcticles .acco__item-head').removeClass('active');
                    $('.arcticles .acco__item-head').removeClass('text');
                }

                setTimeout(function () {
                    self.toggleClass('text')
                }, 500)

            })

        }

        function mobile() {

            $('.arcticles .acco__item-head').click(function () {

                var self = $(this);
                $('.arcticles .acco__item').removeClass('active');
                $('.arcticles .acco__item').removeClass('text');
                if (self.parent().hasClass('active')) {
                    $('.arcticles .acco__item').removeClass('active');
                    $('.arcticles .acco__item').removeClass('text');
                }

            })
        }



        if ($(window).width() > 768) {
            desktop();
        } else {
            mobile();
        }
    }

    articleSwitch();


    function accoDesktop() {

        $('.with-slider').find('.btn, .project-img-slide').click(function (e) {

            var parent = $(this).parents('.acco__item-head');

            if (parent.hasClass('active')) {
                $('.acco__item-head').removeClass('active');
                $('.acco__item-head').next().removeClass('active');
                parent.removeClass('active');
                parent.next().removeClass('active');
                $('.acco__item').animate({
                    marginBottom: '20px'
                },500);

            } else {
                $('.acco__item-head').removeClass('active');
                $('.acco__item-head').next().removeClass('active');
                parent.addClass('active');
                parent.next().addClass('active');
                $('.acco__item').css('margin-bottom', 20);
                parent.parent().animate({
                    marginBottom: parent.next().outerHeight() + 44 + 'px'
                },500);

            }
        })

        $('.acco__item-head').not('.with-slider').click(function (e) {

            if ($(this).hasClass('active')) {
                $('.acco__item-head').removeClass('active');
                $('.acco__item-head').next().removeClass('active');
                $(this).removeClass('active');
                $(this).next().removeClass('active');
                $('.acco__item').animate({
                    marginBottom: '20px'
                },500);

            } else {
                $('.acco__item-head').removeClass('active');
                $('.acco__item-head').next().removeClass('active');
                $(this).addClass('active');
                $(this).next().addClass('active');
                $('.acco__item').css('margin-bottom', 20);
                $(this).parent().animate({
                    marginBottom: $(this).next().outerHeight() + 44 + 'px'
                },500);

            }

        })

    }

    function accoMobile() {

        function stretchAccoBodies() {
            var gridRow = document.querySelector('.portf .grid-row');
            var accoBodies = document.querySelectorAll('.portf .acco__item-body.grid__item')

            // set width equals width gridRow
            for (let i = 0; i < accoBodies.length; i++) {
                var accoBody = accoBodies[i];

                accoBody.style.width = gridRow.offsetWidth + 'px';
            }
        }

        stretchAccoBodies();

        function init() {

            $('.with-slider').find('.btn, .project-img-slide').click(function (e) {

                var parent = $(this).parents('.acco__item-head');

                if (parent.hasClass('active')) {
                    $('.acco__item-head').removeClass('active');
                    $('.acco__item-head').next().removeClass('active');
                    parent.removeClass('active');
                    parent.next().removeClass('active');
                    $('.acco__item').animate({
                        marginBottom: '20px'
                    },200);

                } else {
                    $('.acco__item-head').removeClass('active');
                    $('.acco__item-head').next().removeClass('active');
                    parent.addClass('active');
                    parent.next().addClass('active');
                    $('.acco__item').css('margin-bottom', 20);
                    parent.parent().animate({
                        marginBottom: parent.next().outerHeight() + 44+'px'
                    },200);
                }
            })

            $('.acco__item-head').not('.with-slider').click(function (e) {

                if ($(this).hasClass('active')) {
                    $('.acco__item-head').removeClass('active');
                    $('.acco__item-head').next().removeClass('active');
                    $(this).removeClass('active');
                    $(this).next().removeClass('active');
                    $('.acco__item').animate({
                        marginBottom: '20px'
                    },200);

                } else {
                    $('.acco__item-head').removeClass('active');
                    $('.acco__item-head').next().removeClass('active');
                    $(this).addClass('active');
                    $(this).next().addClass('active');
                    $('.acco__item').css('margin-bottom', 20);
                    $(this).parent().animate({
                        marginBottom: $(this).next().outerHeight() + 44+'px'
                    },200);
                }
            })
        }

        init();
    }

    if ($(window).width() > 1300) {
        accoDesktop();
    } else {
        accoMobile();
    }


    function filter() {
        $('.filters .filters__item').click(function () {
            $('.filters .filters__item').removeClass('active')
            $(this).toggleClass('active')

            // var filterValue = $(this).data().filter;

            // $.each($('.portf__inner .acco__item'), function (i, el) {

            //     if (filterValue === $(this).data().value) {
            //         $(this).fadeIn();
            //     } else {
            //         $(this).fadeOut();
            //     }

            //     if (filterValue === 'ALL') {
            //         $(this).fadeIn();
            //     }
            // })

            // $('.portf__inner .acco__item').addClass()
        })
    }

    filter();

    function checkValidate(callback) {
        var form = $('form');

        $.each(form, function () {
            $(this).validate({
                ignore: [],
                errorClass: 'error',
                validClass: 'success',
                onkeyup: false,
                rules: {
                    name: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    phone: {
                        required: true,
                        phone: true
                    },
                    message: {
                        required: true
                    },
                    pass: {
                        required: true,
                        normalizer: function normalizer(value) {
                            return $.trim(value);
                        }
                    }
                },
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    var placement = $(element).data('error');

                    if (placement) {
                        $(placement).append(error);

                    } else {
                        error.insertBefore(element);
                    }
                },
                messages: {
                    regacept: 'Необходимо согласие',
                    phone: 'Некорректный номер',
                    email: 'Некорректный e-mail'
                }
            });
        });
        jQuery.validator.addMethod('email', function (value, element) {
            return this.optional(element) || /\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6}/.test(value);
        });
        jQuery.validator.addMethod('phone', function (value, element) {
            return this.optional(element) || /\+7\(\d+\)\d{3}-\d{2}-\d{2}/.test(value);
        });

    }
    checkValidate();

    function thxModal() {
        $('.modal-overlay').addClass('modal-overlay--active');
        $('.modal_thx').addClass('modal--active');
    }

    $('.input-wrap').click(function(e){
        $(this).find('span.error').fadeOut(300);
    });

    $('.feedback__form .btn').click(function(e){
        e.preventDefault();
        $('.feedback__form .btn').addClass('disabled');
        if($('.feedback__form').valid()){
            // при успешной отправке
            thxModal();
            // при любой
            $('.feedback__form .btn').removeClass('disabled');
        }
        // для теста
        setTimeout(function(){
            $('.feedback__form .btn').removeClass('disabled');
        },1000);
    });

    $('input[placeholder="Ваш телефон"]').inputmask('+7(999)999-99-99');

    $('.scroll').scrollbar();

    $('.project-img-slider').slick({
        dots: true,
        arrows: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        swipe: false,
        lazyLoad: 'ondemand'
    });
})